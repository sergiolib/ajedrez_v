module Chess_verify (err, b_wins, w_wins, mem_i, adr, read, in1,
mem_in1, in2, mem_in2, mem_o, prom_type, clk, reset);
   output reg err, b_wins, w_wins;
   output reg [4:0] mem_i;
   output reg [5:0] adr;
   output reg 	    read;
   
   input [5:0] 	    in1, in2;
   input [4:0] 	    mem_in1, mem_in2;
   input [4:0] 	    mem_o;
   input [2:0] 	    prom_type;	    
   input 	    reset, clk;
   
   reg 		    curr_player;
   reg 		    end_of_turn;
   wire 	    err1, err2, err3, err4, err5, err6, err7;
   
   
   always @(reset)
     begin
	curr_player <= 1;
	end_of_turn <= 0;
     end
	
   always @(posedge clk)
     begin
	if (end_of_turn == 1)
	  curr_player <= !curr_player;
     end

   always @(err)
     begin
	end_of_turn <= 1;
     end

   always @(err1, err2, err3, err4, err5, err6, err7)
     begin
	err <= 1;
     end
   
   Verify_pawn v1 (.err(err1), .mem_i(mem_i), .adr(adr), .read(read),
   .in1(in1), .mem_in1(mem_in1), .in2(in2), .mem_in2(mem_in2),
   .mem_o(mem_o), .clk(clk), .reset(reset), .curr_player(curr_player),
   .end_of_turn(end_of_turn));
   
   Verify_knight v2 (.err(err2), .mem_i(mem_i), .adr(adr),
   .read(read), .in1(in1), .mem_in1(mem_in1), .in2(in2),
   .mem_in2(mem_in2), .mem_o(mem_o), .clk(clk), .reset(reset),
   .curr_player(curr_player), .end_of_turn(end_of_turn));
   
   Verify_bishop v3 (.err(err3), .mem_i(mem_i), .adr(adr),
   .read(read), .in1(in1), .mem_in1(mem_in1), .in2(in2),
   .mem_in2(mem_in2), .mem_o(mem_o), .clk(clk), .reset(reset),
   .curr_player(curr_player), .end_of_turn(end_of_turn));
   
   Verify_rook v4 (.err(err4), .mem_i(mem_i), .adr(adr), .read(read),
   .in1(in1), .mem_in1(mem_in1), .in2(in2), .mem_in2(mem_in2),
   .mem_o(mem_o), .clk(clk), .reset(reset), .curr_player(curr_player),
   .end_of_turn(end_of_turn));
   
   Verify_queen v5 (.err(err5), .mem_i(mem_i), .adr(adr), .read(read),
   .in1(in1), .mem_in1(mem_in1), .in2(in2), .mem_in2(mem_in2),
   .mem_o(mem_o), .clk(clk), .reset(reset), .curr_player(curr_player),
   .end_of_turn(end_of_turn));
   
   Verify_king v6 (.err(err6), .mem_i(mem_i), .adr(adr), .read(read),
   .in1(in1), .mem_in1(mem_in1), .in2(in2), .mem_in2(mem_in2),
   .mem_o(mem_o), .clk(clk), .reset(reset), .curr_player(curr_player),
   .end_of_turn(end_of_turn));
   
   Verify_check v7 (.err(err7), .mem_i(mem_i), .adr(adr), .read(read),
   .in1(in1), .mem_in1(mem_in1), .in2(in2), .mem_in2(mem_in2),
   .mem_o(mem_o), .clk(clk), .reset(reset), .curr_player(curr_player),
   .end_of_turn(end_of_turn));
   
   Verify_promotion prom (.err(err8), .in2(in2),
   .prom_type(prom_type), .clk(clk), .end_of_game(end_of_turn));
   
endmodule // Chess_verify


