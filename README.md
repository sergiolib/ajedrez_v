Proyecto de verificación de jugadas de ajedrez por hardware
-----------------------------------------------------------

Los módulos de este repositorio conforman un verificador de jugadas de ajedrez.

El grupo de trabajo lo conforman Sergio Liberman y Cristóbal Silva. El
proyecto se realiza para el curso EL6003 dictado en el semestre de
otoño de 2014 en la FCFM de la Universidad de Chile.

El proyecto no está licenciado.