/* Modulo de verificacion de jugadas de ajedrez
  * Por: Sergio Liberman y Cristobal Silva (EL6003/Julio2014) */

  module Verification(err, b_wins, w_wins, mem_i, adr, read, mem_o, pos1, pos2,
		      prom_type, clk, reset);
   
   /* Outputs del modulo */
   output reg err, b_wins, w_wins;

   /* Outputs de consulta a la memoria */
   output reg [4:0] mem_i;
   output reg [5:0] adr;
   output reg       read;

   /* Inputs del modulo */
   input [5:0] 	    pos1, pos2;
   input [2:0] 	    prom_type; 	    
   input 	    clk;
   input 	    reset;

   /* Input desde la memoria */   
   input [4:0] 	    mem_o;

   /* Valores de entrada */
   reg [5:0] 	    in1;
   reg [5:0] 	    in2;
   reg [4:0] 	    mem_in1;
   reg [4:0] 	    mem_in2;
   reg 		    reading; // Selector de señal de lectura
 
   always @(pos1) // Consulta a memoria por pieza de entrada
     begin
	adr <= pos1;
	read <= 1;
	in1 <= pos1;
	err <= 0;	
     end

   always @(pos2) // Consulta a memoria por pieza de salida
     begin
	adr <= pos2;
	read <= 1;
        in2 <= pos2;
	reading <= 1;
     end
   
   always @(mem_o)
     begin
	if (reading == 0)
	  mem_in1 <= mem_o; // Lectura realizada de pos1
	else
	  mem_in2 <= mem_o; // Lectura realizada de pos2
     end

   Chess_verify c (.err(err), .b_wins(b_wins), .w_wins(w_wins),
   .mem_i(mem_i), .adr(adr), .read(read), .in1(in1),
   .mem_in1(mem_in1), .in2(in2), .mem_in2(mem_in2), .mem_o(mem_o),
   .prom_Type(prom_type), .clk(clk), .reset(reset));
   
   /* Inicializacion de registros */
   always @(reset)
     begin
	err <= 1'b0;
	b_wins <= 1'b0;
	w_wins <= 1'b0; 
	
	mem_i <= 5'hx;
	adr <= 6'oxx;
	read <= 1'b1;
	
     end // always @ (reset)
   
endmodule // Verification
