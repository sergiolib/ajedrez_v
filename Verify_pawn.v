module Verify_pawn (err, mem_i, adr, read, in1, mem_in1, in2, mem_in2, mem_o, clk, reset, curr_player, end_of_turn);

   output reg       err;
   output reg [4:0] mem_i;
   output reg [5:0] adr;
   output reg 	    read;

   input [5:0] 	    in1, in2;
   input [4:0] 	    mem_in1, mem_in2;
   input [4:0] 	    mem_o;
   input 	    clk, reset, curr_player;

   inout 	    end_of_turn;

   reg 		    c1, c2;
   reg [2:0] 	    t1, t2;
   reg 		    good;

   reg [2:0] 	    line_in1, line_in2, col_in1, col_in2;
   reg 		    pend1;
   
   In_line l1 (.line(line_in1), .col(col_in1), .pos(in1)), .clk(clk));
   In_line l2 (.line(line_in2), .col(col_in2), .pos(in2)), .clk(clk));

   always @(posedge clk)
     begin
	c1 <= (mem_in1 & 5'h10) >> 4;
	c2 <= (mem_in2 & 5'h10) >> 4;
	t1 <= (mem_in1 & 5'h0e) >> 1;
	t2 <= (mem_in2 & 5'h0e) >> 1;

	if (t1 == 1 && end_of_turn == 0)
	  begin
	     // Verificacion movimiento sin comer
	     
	     // Acciones de movimiento de 2 espacios blancas (p1)    
	     if (t2 == 0 and c1 == 1 and line_in1 == 1 and line_in2 == 3 and pend1 == 0)
	       begin
		  mem_i <= 5'd0;
		  adr <= in1;
		  read <= 0;
		  pend1 <= 1;
	       end

	     // Acciones de movimiento de 2 espacios blancas (p2)
	     if (t2 == 0 and c1 == 1 and line_in1 == 1 and line_in2 == 3 and pend1 == 1)
	       begin
		  mem_i <= 5'h13;
		  adr <= in2;
		  read <= 0;
		  good <= 1;
		  pend1 <= 0;
	       end

	     // Acciones fila de movimiento de 2 espacios negras (p1)	     
	     if (t2 == 0 and c1 == 0 and line_in1 == 6 and line_in2 == 4 and pend1 == 0)
	       begin
		  mem_i <= 5'd0;
		  adr <= in1;
		  read <= 0;
		  pend1 <= 1;
	       end

	     // Acciones fila de movimiento de 2 espacios negras (p2)
	     if (t2 == 0 and c1 == 0 and line_in1 == 6 and line_in2 == 4 and pend1 == 1)
	       begin
		  mem_i <= 5'h03;
		  adr <= in2;
		  read <= 0;
		  good <= 1;
		  pend1 <= 0;
	       end

	     // Acciones avance de 1 espacio blancas (p1)
	     if (t2 == 0 and c1 == 1 and (in2 - in1 == 1) and pend1 == 0)
	       begin
		  mem_i <= 5'd0;
		  adr <= in1;
		  read <= 0;
		  pend1 <= 1;
	       end

	     // Acciones avance de 1 espacio blancas (p2)
	     if (t2 == 0 and c1 == 1 and (in2 - in1 == 1) and pend1 == 1)
	       begin
		  mem_i <= 5'h13;
		  adr <= in2;
		  read <= 0;
		  good <= 1;
		  pend1 <= 0;
	       end
	   
	     // Acciones avance de 1 espacio negras (p1)
	     if (t2 == 0 and c1 == 0 and (in1 - in2 == 1) and pend1 == 0)
	       begin
		  mem_i <= 5'd0;
		  adr <= in1;
		  read <= 0;
		  pend1 <= 1;
	       end

	     // Acciones avance de 1 espacio negras (p2)
	     if (t2 == 0 and c1 == 0 and (in1 - in2 == 1) and pend1 == 1)
	       begin
		  mem_i <= 5'h03;
		  adr <= in2;
		  read <= 0;
		  good <= 1;
		  pend1 <= 0;
	       end

	     // Verificacion movimiento con comer
    
	     // Accion para comer de las blancas (p1)
	     if (t2 != 0 and c1 == 1 and ((in1 + 9 == in2) or (in1 - 7 == in2)) and pend1 == 0)
	       begin
		  mem_i <= 5'd0;
		  adr <= in1;
		  read <= 0;
		  pend1 <= 1;

	       end

	     // Accion para comer de las blancas (p2)
	     if (t2 != 0 and c1 == 1 and ((in1 + 9 == in2) or (in1 - 7 == in2)) and pend1 == 1)
	       begin
		  mem_i <= 5'h13;
		  adr <= in2;
		  read <= 0;
		  good <= 1;
		  pend1 <= 0;		  
	       end

	     // Accion para comer de las negras (p1)
	     if (t2 != 0 and c1 == 0 and ((in1 - 9 == in2) or (in1 + 7 == in2)) and pend1 == 0)
	       begin
		  mem_i <= 5'd0;
		  adr <= in1;
		  read <= 0;
		  pend1 <= 1;
	       end

	     // Accion para comer de las negras (p2)
	     if (t2 != 0 and c1 == 0 and ((in1 - 9 == in2) or (in1 + 7 == in2)) and pend1 == 1)
	       begin
		  mem_i <= 5'h03;
		  adr <= in2;
		  read <= 0;
		  good <= 1;
		  pend1 <= 0;		  
	       end
    
	     // Si no se ejecuto nada de lo anterior, error
	     if (good == 0) 
	       begin
		  err <= 1;		  
	       end	     

	     if (good == 1 and ((c1 == 1 and (in2 == 7 or in2 == 15 or
	     in2 == 23 or in2 == 31 or in2 == 39 or in2 == 47 or in2
	     == 55 or in2 == 63)) or (c1 == 0 and (in2 == 0 or in2 ==
	     8 or in2 == 16 or in2 == 24 or in2 == 32 or in2 == 40 or
	     in2 == 48 or in2 == 56))))	       
	       end_of_turn <= 0; // Deja la posibilidad de verificar
				 // coronación
	     else
	       end_of_turn <= 1;	     

	  end // if (t1 == 1 && end_of_turn == 0)
	
     end // always @ (posedge clk)

endmodule // Verify_pawn
