/* Modulo principal de juego de ajedrez
 * Por: Sergio Liberman y Cristobal Silva (EL6003/Mayo2014) */

module top (err, b_wins, w_wins, pos1, pos2, prom_type, clk, reset);
   /* Entradas y salidas */
   input [5:0]  pos1;
   input [5:0] 	pos2;
   input [2:0] 	prom_type;
   input 	clk;
   input 	reset;

   output reg 	err, b_wins, w_wins;

   /* Regs y wires */
   wire [5:0] 	mem_o;
   wire [5:0] 	mem_i;
   wire [5:0] 	adr;
   wire 	read;

   // Memoria compartida
   Mem mem (.Obus(mem_o), .Ibus(mem_i), .Read(read), .Clk(clk),
	       .Reset(reset));

   // Verificacion
   Verification v (.err(err), .b_wins(b_wins), .w_wins(w_wins), .mem_i(mem_i),
		  .adr(adr), .read(read), .mem_o(mem_o), .pos1(pos1), 
		  .pos2(pos2), .prom_type(prom_type), .clk(clk), .reset(reset));
   
endmodule // top
